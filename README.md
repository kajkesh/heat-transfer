## Heat Transfer

Program for simulating heating process of forging tools.

---

## Overview

This desktop app allows one to simulate heating process of six defferent types of forging tools.
All tool types are axially symmetric and differ with number of inner and outer diameters.
A user can specify tool dimensions and process parameters such as ambient temperature or specific
heat of material used. The program generates finite element mesh for specified tool, then finite
element method is used to calculate the time needed for the tool to reach furnace temperature in
all its volume.
This program can be used in industry for optimizing heating time and enhancing mechanical properties
of tools subjected to hardening by ensuring that required material structure is obtained during
pre-hardening heating.

---

## Platforms

Windows

---

## Installation

---

## Usage

---

## Tests

---

## Known Issues

---